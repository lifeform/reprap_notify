package main

import (
	"fmt"
	"github.com/spf13/viper"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

func serveConfig(w http.ResponseWriter, r *http.Request) {
	b, err := ioutil.ReadFile("rrnotify.yaml")
	if err != nil {
		log.Printf("%v", err)
	}

	t := template.New("configuration")
	if _, err := os.Stat("./index.tmpl"); err == nil {
		t, err = template.ParseFiles("./index.tmpl")
		if err != nil {
			log.Printf("%v", err)
		}
	} else {
		t, err = t.Parse(`<!DOCTYPE html><html lang="en" ><head> <meta charset="UTF-8"> <title>RepRap Notify Config Editor</title> <style>body{margin: 10;}#textbox{width: 90vw; height: 90vh; border: 1px solid #888; padding: 5px; resize: none;}</style></head><body> <div id="form-container" class="container"> <form action="/submit" method="GET"> <textarea name="content" id="textbox">{{.Content}}</textarea> <div> <input type="submit" value="Send new configuration"> </div></form> <button value="Reload Configuration" onClick="window.location.href=window.location.href">Reload Configuration</button> </div></body></html>`)
		if err != nil {
			log.Printf("%v", err)
		}
	}
	tpl := tpl{Content: string(b)}
	t.Execute(w, tpl)
}

func getNewConfig(w http.ResponseWriter, r *http.Request) {
	content := ``

	switch r.Method {
	case "POST":
		r.ParseForm()
		content = r.FormValue("content")
	case "GET":
		content = r.URL.Query().Get("content")
	}

	if len(content) > 0 {
		data := []byte(content)
		err := ioutil.WriteFile("./rrnotify.yaml", data, 0600)
		if err != nil {
			log.Printf("%v", err)
		}
	}

	http.Redirect(w, r, "/", 301)
}

func httpServer() {
	if viper.GetBool(`http_server.enabled`) {
		http.HandleFunc("/", serveConfig)
		http.HandleFunc("/submit", getNewConfig)

		if err := http.ListenAndServe(fmt.Sprintf(":%v", viper.GetInt(`http_server.port`)), nil); err != nil {
			panic(err)
		}
	}
}
