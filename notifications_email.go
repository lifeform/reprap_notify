package main

import (
	"log"

	"github.com/spf13/viper"
	"gopkg.in/gomail.v2"
)

func sendEmail(messageTitle string, messageText string) {
	m := gomail.NewMessage()
	m.SetHeader("From", viper.GetString(`notifications.email.email`))
	m.SetHeader("To", viper.GetString(`notifications.email.email`))
	m.SetHeader("Subject", messageTitle)
	m.SetBody("text/plain", messageText)

	d := gomail.NewDialer(
		viper.GetString(`notifications.email.server`),
		viper.GetInt(`notifications.email.port`),
		viper.GetString(`notifications.email.username`),
		viper.GetString(`notifications.email.password`),
	)

	if err := d.DialAndSend(m); err != nil {
		log.Printf("Error while sending email: %v", err)
	}

}
