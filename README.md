# RepRap Notifier [![pipeline status](https://gitlab.com/Toriniasty/reprap_notify/badges/master/pipeline.svg)](https://gitlab.com/Toriniasty/reprap_notify/commits/master)

This is small software that 'fixes' lack of ability to notify user on the status of his print on RepRap firmware for Duet motherboards.

## Integrations supported

* Pushbullet
* Pushover
* Email
* Telegram
* Slack

## How to start

1. Download latest release appropriate for your operating system.  
For any linux/bsd/osx you will have to give executable permissions for the binary and run in from terminal.
2. Grab sample config file from the website and rename it to rrnotify.yaml
3. Edit it and put your proper parameters in.
4. Run

## HTTP server

Now you can enable bulit-in http server to have builtin webconfig editor.

## Configuration file explained

File uses YAML syntax format so indentation matters!  
Comments are after # in each section.

```yaml
# Connection details section
connection:
# Hostname or IP of your 3d printer e.g. 192.168.0.249 or my3dprinter.local
  host: 192.168.0.249
# How often we should poll printer for status change
  polling_time: 30
# How often to reset counters(this is useful if you will turn off printer without finishing print)
  error_count: 5

# Internal webserver to edit config
http_server:
# Is it enabled? true or false
  enabled: false
# Port to listen on
  port: 8080

# Notification section configuration
notifications:
# Pushbullet integration
  pushbullet:
# Is it enabled? true or false
    enabled: false
# Pushbullet generated apikey, you need to put it here to make the integration working. You can get one from [Pushbullet Account Settings Page](https://www.pushbullet.com/#settings/account)
    apikey: <apikey>
# To push to specific device put its name over here. Otherwise just remove it or leave blank
    device:
# When to send notification. Valid options are: printing, paused, finished. If you don't need one, just remove it.
    when:
      - printing
      - paused
      - finished
# Pushover integration
  pushover:
# Is it enabled? true or false
    enabled: false
# Your application API key
    apikey: <apikey>
# Your user key, found on a profile page
    userkey: <userkey>
# When to send notification. Valid options are: printing, paused, finished. If you don't need one, just remove it.
    when:
      - printing
      - paused
      - finished
# Email integration
  email:
# Is it enabled? true or false
    enabled: false
# Your SMTP server
    server: <server>
# Your SMTP port
    port: <port>
# Your SMTP username
    username: <username>
# Your SMTP password
    password: <password>
# Where to send email
    email: <youremailaddress>
# When to send notification. Valid options are: printing, paused, finished. If you don't need one, just remove it.
    when:
      - printing
      - paused
      - finished
# Telegram integration
  telegram:
# Is it enabled? true or false
    enabled: true
# Your bot token id
    token: <some_token>
# Your channel id taken from getUpdates page
    chatid: <chat_id>
    when:
      - printing
      - paused
      - finished

# Actions are system commands that are executed when system will detect state change
actions:
  when:
    printing:
    paused:
    finished:
      - sleep 180 && curl "http://192.168.0.32/cmnd=Power%20Off"
      - ./cleanup.sh
```

## Telegram quick configuration

1. Create new bot via `@Botfather` with /newbot message
2. Create new channel, the type of the channel is up to you
3. Add your newly created bot to the channel as administrator with just only post messages rights
4. Get your new bot chat id by going to this website:
`https://api.telegram.org/bot<YOUR_TOKEN>/getUpdates`
It will be here as: result.message.chat.id
(if the result will be empty try sending simple message to that channel)
5. Put your token and chat id in the configuration
